#!/bin/bash

# Script to create some links

function link(){
  sudo rm $1 2>> /dev/null
  if [ -d $2 ]; then
    sudo ln -sf $2 $1
  else
    echo "Cannot find $2, ignoring"
  fi
}

case $1 in
"eclipse"|"luna")
  link /storage/progs/luna /storage/progs/eclipse-4.4/eclipse
;;
"gf4")
  link /jeeservers/runtimes/gf4 /jeeservers/runtimes/glassfish4
;;
"mvn")
link /opt/maven /opt/apache-maven-3.2.3
;;
"gradle")
link /opt/gradle /opt/gradle-2.2
;;
"openjdk7")
link /jeeservers/java/openjdk-1.7 /usr/lib64/jvm/java-1.7.0-openjdk-1.7.0
;;
"openjre7")
link /jeeservers/java/openjdk-jre-1.7 /usr/lib64/jvm/java-1.7.0-openjdk-1.7.0/jre
;;
"jdk8")
link /jeeservers/java/jdk8 /jeeservers/java/oracle-1.8
;;
"jre8")
link /jeeservers/java/jre8 /jeeservers/java/oracle-jre-1.8
;;
"jdk7")
link /jeeservers/java/jdk7 /jeeservers/java/openjdk-1.7
;;
"jre7")
link /jeeservers/java/jre7 /jeeservers/java/openjdk-jre-1.7
;;
*)
echo "Unknown goal: $1"
;;
esac
