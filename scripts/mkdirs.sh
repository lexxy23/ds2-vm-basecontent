#!/bin/bash

# expect that this is run as root!!
echo "Setting up jeeservers directory and some links"

sudo install -m 755 -d /jeeservers/java
sudo install -m 755 -d /jeeservers/instances
sudo install -m 755 -d /jeeservers/runtimes
sudo install -m 755 -d /storage/progs
sudo install -o $USER -m 755 -d /storage/homes/$USER
sudo install -m 755 -d /storage/vm
sudo install -m 755 -d /storage/mavenrepo/3
sudo install -m 755 -d /storage/hdfs
sudo install -m 755 -d /storage/mavendocs

# for MacOS
sudo install -m 755 -d /opt
