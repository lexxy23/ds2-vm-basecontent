#!/bin/bash

# Script to install the systemd files.

sudo install -m 640 -o root -g root ../systemd/clear-userdata.service /etc/systemd/system/clear-userdata.service

if [ -n "$1" ]; then
  sudo cp ../setts/$1/*.env /etc/
fi

sudo systemctl daemon-reload
