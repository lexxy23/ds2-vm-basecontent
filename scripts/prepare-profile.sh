#!/bin/bash

# Script to setup the ~/.ds2-profile file

P=$HOME/.ds2-profile

echo "remove old file"
rm $P 2>> /dev/null

echo "setup exports"
../envs/print_envs.sh $1 >> $P

if [ $# -gt 0 ]; then
  echo "Adding data from $1-profile.txt.."
  echo "#" >> $P
  echo "# adding data from $1-profile.txt" >> $P
  cat ../setts/$1-profile.txt >> $P
fi

echo "write last line"
echo "" >> $P
