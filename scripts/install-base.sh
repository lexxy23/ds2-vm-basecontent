#!/bin/bash

GRADLE="https://downloads.gradle.org/distributions/gradle-2.2.1-all.zip"
MAVEN="http://apache.osuosl.org/maven/maven-3/3.2.5/binaries/apache-maven-3.2.5-bin.tar.gz"
JDK8="http://download.oracle.com/otn-pub/java/jdk/8u25-b17/jdk-8u25-linux-x64.tar.gz"
JRE8="http://download.oracle.com/otn-pub/java/jdk/8u25-b17/server-jre-8u25-linux-x64.tar.gz"
GSUTIL="https://storage.googleapis.com/pub/gsutil.tar.gz"
LOGSTASH="https://download.elasticsearch.org/logstash/logstash/logstash-1.4.2.tar.gz"
KIBANA="https://download.elasticsearch.org/kibana/kibana/kibana-3.1.2.tar.gz"
JBOSS="http://download.jboss.org/wildfly/8.2.0.Final/wildfly-8.2.0.Final.tar.gz"
ES="https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.4.2.tar.gz"
GF="http://dlc.sun.com.edgesuite.net/glassfish/4.1/release/glassfish-4.1.zip"
ECLIPSE="http://ftp-stud.fht-esslingen.de/pub/Mirrors/eclipse/technology/epp/downloads/release/luna/R/eclipse-jee-luna-R-linux-gtk-x86_64.tar.gz"

#if [ ] then
#fi

umask 027

function dl(){
  local DL=$1
  local T=$2
  # wget -c -t 5 -w 15 -p /tmp/wget-download.log -O $T $DL
  curl -C - --connect-timeout 30 -o $T $DL
}

function oracleDl(){
  local DL=$1
  local T=$2
  wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" -c -t 5 -w 15 -p /tmp/wget-download.log -O $T $DL
}

function sudoUnzip(){
  local ZIP=$1
  local T=$2
  umask 022
  sudo unzip -o -d $T $ZIP
}

function sudoTgz(){
  local TGZ=$1
  local T=$2
  umask 022
  sudo mkdir -p $T 2>> /dev/null
  echo "Entpacke $TGZ nach $T.."
  sudo tar -xzf $TGZ -C $T
}

function sudoTbz(){
  local TGZ=$1
  local T=$2
  umask 022
  sudo tar -xjf $TGZ -C $T
}

function link(){
  local S=$1
  local T=$2
  # echo "Please use link-base.sh for linking directories to common aliases!"
  echo "Linking $S to $T"
  sudo rm $T 2>> /dev/null
  sudo ln -sf $S $T
}

case "$1" in
"eclipse")
  dl $ECLIPSE /tmp/eclipse.tgz
  sudoTgz /tmp/eclipse.tgz /storage/progs/eclipse-4.4
;;
"gf")
dl $GF /tmp/gf.zip
sudoUnzip /tmp/gf.zip /jeeservers/runtimes
;;
"tomcat7")
dl "http://mirrors.ae-online.de/apache/tomcat/tomcat-7/v7.0.57/bin/apache-tomcat-7.0.57.tar.gz" /tmp/tc7.tgz
sudoTgz /tmp/tc7.tgz /jeeservers/runtimes/
link /jeeservers/runtimes/apache-tomcat-7.0.57 /jeeservers/runtimes/tomcat7
;;
"tomcat8")
dl "http://apache.openmirror.de/tomcat/tomcat-8/v8.0.17/bin/apache-tomcat-8.0.17.tar.gz" /tmp/tc8.tgz
sudoTgz /tmp/tc8.tgz /jeeservers/runtimes/
link /jeeservers/runtimes/apache-tomcat-8.0.17 /jeeservers/runtimes/tomcat8
;;
"elasticsearch")
  dl $ES /tmp/es.tgz
  sudoTgz /tmp/es.tgz /storage/progs
  link /storage/progs/elasticsearch-1.4.2 /storage/progs/elasticsearch
;;
"gradle")
  dl $GRADLE /tmp/gradle.zip
  sudoUnzip /tmp/gradle.zip /opt
;;
"mvn")
  dl $MAVEN /tmp/mvn.tgz
  sudoTgz /tmp/mvn.tgz /opt
  link /opt/apache-maven-3.2.5 /opt/maven
;;
"jdk8")
  oracleDl $JDK8 /tmp/jdk8.tgz
  sudoTgz /tmp/jdk8.tgz /jeeservers/java
  link /jeeservers/java/jdk1.8.0_25 /jeeservers/java/oracle-1.8
  link /jeeservers/java/jdk1.8.0_25/jre /jeeservers/java/oracle-jre-1.8
;;
"jre8")
  oracleDl $JRE8 /tmp/jre8.tgz
  sudoTgz /tmp/jre8.tgz /jeeservers/java
;;
"jce8")
  oracleDl "http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip" /tmp/jce8.zip
  sudoUnzip /tmp/jce8.zip /tmp
  echo "Files have been created in /tmp/UnlimitedJCEPolicyJDK8/. Copy any jar to the JRE/lib/security folder."
  sudo cp /tmp/UnlimitedJCEPolicyJDK8/*.jar /jeeservers/java/oracle-1.8/jre/lib/security/
;;
"jdk7")
  oracleDl "http://download.oracle.com/otn-pub/java/jdk/7u72-b01/jdk-7u72-linux-x64.tar.gz" /tmp/jdk7.tgz
  sudoTgz /tmp/jdk7.tgz /jeeservers/java
  link /jeeservers/java/jdk1.7.0_72 /jeeservers/java/oracle-1.7
  link /jeeservers/java/jdk1.7.0_72/jre /jeeservers/java/oracle-jre-1.7
;;
"mvn209")
  dl http://archive.apache.org/dist/maven/binaries/apache-maven-2.0.9-bin.tar.bz2 /tmp/mvn_209.tar.bz2
  sudoTbz /tmp/mvn_209.tar.bz2 /opt
;;
"mvn208")
  dl http://archive.apache.org/dist/maven/binaries/apache-maven-2.0.8-bin.tar.bz2 /tmp/mvn_208.tar.bz2
  sudoTbz /tmp/mvn_208.tar.bz2 /opt
;;
"gsutil")
  dl $GSUTIL /tmp/gsutil.tgz
  sudoTgz /tmp/gsutil.tgz /opt
;;
"logstash")
  dl $LOGSTASH /tmp/logstash.tgz
  sudoTgz /tmp/logstash.tgz /opt
  link /opt/logstash-1.4.2 /opt/logstash
;;
"kibana")
  dl $KIBANA /tmp/kibana.tgz
  sudoTgz /tmp/kibana.tgz /opt
  link /opt/kibana-3.1.2 /opt/kibana
;;
"wildfly")
  dl $JBOSS /tmp/wildfly.tgz
  sudoTgz /tmp/wildfly.tgz /jeeservers/runtimes
  link /jeeservers/runtimes/wildfly-8.2.0.Final /jeeservers/runtimes/wildfly8
  link /jeeservers/runtimes/wildfly8 /jeeservers/runtimes/wildfly
;;
*)
echo "To install some artifacts into wellknown destinations"
echo "Currently supported: gradle|mvn|mvn209|mvn208"
echo "Unknown: $1"
;;
esac
