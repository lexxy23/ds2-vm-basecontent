#!/bin/bash

# copy some well known aliases to home

ALIAS_TARGET="$HOME/.alias"

# echo "AT auf $ALIAS_TARGET"
# touch $ALIAS_TARGET 2>> /dev/null
cat ../setts/common-aliases.txt > $ALIAS_TARGET

if [ $# -gt 0 ]; then
  echo "# adding $1 data" >> $ALIAS_TARGET
  cat ../setts/$1-aliases.txt >> $ALIAS_TARGET
fi

rm $HOME/.bash_aliases 2>> /dev/null
ln -s $ALIAS_TARGET $HOME/.bash_aliases
