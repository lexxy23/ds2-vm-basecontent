#!/bin/bash

CURRDIRPWD=`pwd`

CURRDIR="${CURRDIRPWD}"
NEWOWNER=""
NEWGRP=""
DRYRUN=0

while getopts ":p:o:g:dh" OPTION ; do
  case "$OPTION" in
    h) echo "Params:"
       echo "-p DIRECTORY=the directory to change"
       echo "-o USERNAME = the new owner"
       echo "-g GROUP = the new owner group"
       echo "-d = dry run"
       echo "-h = this help"
       exit 2
       ;;
    p) CURRDIR=$OPTARG ;;
    o) NEWOWNER=$OPTARG;;
    g) NEWGRP=$OPTARG;;
    d) DRYRUN=1 ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
    *) echo"Unbekannter Parameter" exit 1
    ;;
  esac
done

echo "Directory to change is $CURRDIR, new owner is \"$NEWOWNER\" and new group is \"$NEWGRP\""

if [ $DRYRUN -gt 0 ]; then
  echo "Dryrun is enabled: no real changes will be done"
else
  echo "Changes will be applied."
fi

echo "STRG+C zum Abbrechen? Ansonsten Enter, um es so zu machen!"
read BLAA

echo "Scanning directory"
FILES=`find $CURRDIR -type f`
DIRS=`find $CURRDIR -type d`

echo "Performing chmod.."
for file in $FILES; do
  #echo "File $file"
  if [ -x "$file" ]; then
    if [ "$DRYRUN" -gt 0 ]; then
      echo "exec $file"
    else
      sudo chmod 775 $file
    fi
  else
    if [ "$DRYRUN" -gt 0 ]; then
      echo "non exec $file"
    else
      sudo chmod 664 $file
    fi
  fi
  if [ -n "$NEWOWNER" ] || [ -n "$NEWGRP" ]; then
    # echo "Changing owner of $file to $NEWOWNER, $NEWGRP"
    if [ "$DRYRUN" -gt 0 ]; then
      echo "dryrun"
    else
      sudo chown ${NEWOWNER}:${NEWGRP} "$file"
    fi
  fi
done

echo "Updating directories.."
for dir in $DIRS; do
  # echo "Updating directory $dir"
  if [ "$DRYRUN" -gt 0 ]; then
    echo "dryrun"
  else
    sudo chmod 775 $dir
  fi
  #
  if [ -n "$NEWOWNER" ] || [ -n "$NEWGRP" ]; then
    # echo "Changing owner of $file to $NEWOWNER, $NEWGRP"
    if [ "$DRYRUN" -gt 0 ]; then
      echo "dryrun"
    else
      sudo chown ${NEWOWNER}:${NEWGRP} "$dir"
    fi
  fi
done
