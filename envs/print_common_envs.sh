#!/bin/bash
# This script prints out some common ENV variables with control values, no paths

MAVEN_OPTS="-Xmx1024m -Dmaven.artifact.threads=3"
GRADLE_USER_HOME=/storage/homes/$USER/gradleRepo
HISTCONTROL=ignoredups
HISTIGNORE="&:[bf]g:exit:[ \t]*"
NSS_SSL_CBC_RANDOM_IV=0

sudo mkdir -p $GRADLE_USER_HOME

envs="MAVEN_OPTS GRADLE_USER_HOME HISTCONTROL HISTIGNORE"

for var in $envs; do
  content=`echo ${!var}`
  echo "export $var=\"$content\""
done
