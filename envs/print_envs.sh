#!/bin/bash

# Script to print any found known ENVs

NEWP=""

function addToPath(){
  local D=$1
  local content=`echo ${!D}`
  local contentBin="${content}/bin"
  # echo "# $D=$contentBin or $content"
  if [ -d "$contentBin" ]; then
    NEWP="$contentBin:$NEWP"
  elif [ -d "$content" ]; then
    NEWP="$content:$NEWP"
  else
    echo "# $D not found"
  fi
}

function exportIf(){
  local E=$1
  local content=`echo ${!E}`
  if [ -d "$content" ]; then
    echo "export $E=$content"
  fi
}

ANDROID_HOME=$HOME/tools/android
ANT_HOME=/opt/ant
JDK8_HOME=/jeeservers/java/jdk8
JDK7_HOME=/jeeservers/java/jdk7
JDK6_HOME=/jeeservers/java/jdk6
JRE8_HOME=/jeeservers/java/jre8
JRE7_HOME=/jeeservers/java/jre7
JRE6_HOME=/jeeservers/java/jre6
JAVA_HOME=$JDK8_HOME
JRE_HOME=$JRE8_HOME
GRADLE_HOME=/opt/gradle
MAVEN_HOME=/opt/maven
MAVEN_208_HOME=/opt/apache-maven-2.0.8
MAVEN_209_HOME=/opt/apache-maven-2.0.9
NB_HOME=/usr/local/netbeans
IVY_HOME=/opt/ivy
INSTALL4J_JAVA_HOME_OVERRIDE=${JAVA_HOME}
IDEA_JDK=$JDK7_HOME
HADOOP_PREFIX=/opt/hadoop
HADOOP_LOG_DIR=/tmp
GRADLE_USER_HOME="$HOME/gradleRepo"
MAVEN_REPO_HOME=$HOME/mavenRepo
JDK7_OPENJDK_HOME=/jeeservers/java/openjdk-1.7
JRE7_OPENJDK_HOME=/jeeservers/java/openjdk-jre-1.7
JDK7_ORACLE_HOME=/jeeservers/java/oracle-1.7
JRE7_ORACLE_HOME=/jeeservers/java/oracle-jre-1.7
GSUTIL_HOME=/opt/gsutil
LUNA_HOME=/storage/progs/luna
ECLIPSE_HOME=$LUNA_HOME
POSTGRESQL_HOME=/opt/postgresql

if [ $# > 0 ]; then
  echo "# Adding content from $1-preenv.txt"
  source ../setts/$1-preenv.txt 2>> /dev/null
fi

envs="ANDROID_HOME ANT_HOME JAVA_HOME MAVEN_HOME GRADLE_HOME IVY_HOME GRADLE_USER_HOME MAVEN_REPO_HOME HADOOP_LOG_DIR HADOOP_PREFIX INSTALL4J_JAVA_HOME JDK7_HOME JDK8_HOME JRE7_HOME JRE8_HOME JRE_HOME JDK6_HOME JRE6_HOME IDEA_JDK JDK7_OPENJDK_HOME JRE7_OPENJDK_HOME JDK7_ORACLE_HOME JRE7_ORACLE_HOME MAVEN_208_HOME MAVEN_209_HOME ECLIPSE_HOME LUNA_HOME POSTGRESQL_HOME"
pathExts="JDK6_HOME JRE6_HOME JDK7_HOME JRE7_HOME MAVEN_HOME GRADLE_HOME ANT_HOME JAVA_HOME JRE_HOME GSUTIL_HOME POSTGRESQL_HOME"

for var in $envs; do
  exportIf $var
done

for var in $pathExts; do
  addToPath $var
done

echo "export PATH=$NEWP:$PATH"
