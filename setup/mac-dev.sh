#!/bin/bash

# simple dev machine for MacOS

../scripts/mkdirs.sh

../scripts/install-base.sh mvn
../scripts/install-base.sh gradle
../scripts/install-base.sh elasticsearch
../scripts/install-base.sh tomcat7
../scripts/install-base.sh tomcat8
../scripts/install-base.sh logstash
../scripts/install-base.sh kibana
../scripts/install-base.sh wildfly
../scripts/install-base.sh gf

#linux:
#../scripts/install-base.sh jdk8
#../scripts/install-base.sh jre8

../scripts/link-base.sh mvn
../scripts/link-base.sh gradle
../scripts/link-base.sh gf4
../scripts/link-base.sh luna
../scripts/link-base.sh gradle

../scripts/prepare-profile.sh mac-dev
../scripts/prepare-aliases.sh mac-dev
#../scripts/install-scripts.sh mac-dev

echo "source .ds2-profile" > $HOME/.bash_profile
echo "source .alias" >> $HOME/.bash_profile
