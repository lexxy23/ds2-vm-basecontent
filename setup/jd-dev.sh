#!/bin/bash

# Jd/fn script

../scripts/mkdirs.sh

../scripts/install-base.sh mvn
../scripts/install-base.sh mvn208
../scripts/install-base.sh mvn209
../scripts/install-base.sh gradle
../scripts/install-base.sh jdk8
../scripts/install-base.sh jre8
../scripts/install-base.sh jdk7
../scripts/install-base.sh jdk6
../scripts/install-base.sh jre6

../scripts/link-base.sh mvn
../scripts/link-base.sh gradle
../scripts/link-base.sh openjdk7
../scripts/link-base.sh openjre7
../scripts/link-base.sh jdk8
../scripts/link-base.sh jdk7
../scripts/link-base.sh jre8
../scripts/link-base.sh jre7
../scripts/link-base.sh jdk6
../scripts/link-base.sh jre6

../scripts/prepare-profile.sh jd-dev
../scripts/prepare-aliases.sh jd-dev
../scripts/install-scripts.sh jd-dev
