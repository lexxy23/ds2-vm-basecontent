#!/bin/bash

# simple dev machine

../scripts/mkdirs.sh

../scripts/install-base.sh mvn
../scripts/install-base.sh gradle
../scripts/install-base.sh jdk8
# ../scripts/install-base.sh jre8

# ../scripts/link-latest-gradle.sh
# ../scripts/link-latest-maven.sh
../scripts/link-base.sh jdk8
../scripts/link-base.sh jdk7
../scripts/link-base.sh jre8
../scripts/link-base.sh jre7

../scripts/prepare-profile.sh simple-dev
../scripts/prepare-aliases.sh simple-dev
../scripts/install-scripts.sh simple-dev
#../scripts/install-systemd.sh simple-dev
